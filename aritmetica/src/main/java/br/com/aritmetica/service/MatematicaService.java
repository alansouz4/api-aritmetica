package br.com.aritmetica.service;

import br.com.aritmetica.DTO.EntradaDTO;
import br.com.aritmetica.DTO.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO){
        int numero = 0;
        for(int n: entradaDTO.getNumeros() ){
            numero += n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO dividi(EntradaDTO entradaDTO){
        int numero = 0;
        for(int n: entradaDTO.getNumeros()){
            numero /= n;
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setResultado(numero);
        return respostaDTO;
    }

    public RespostaDTO multiplica(EntradaDTO entradaDTO){
        int numero = 0;
        for(int n : entradaDTO.getNumeros()){
            numero *= n;
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setResultado(numero);
        return respostaDTO;
    }

    public RespostaDTO subtrai(EntradaDTO entradaDTO){
        int numero = 0;
        for(int n : entradaDTO.getNumeros()){
            numero -= n;
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setResultado(numero);
        return respostaDTO;
    }
}
