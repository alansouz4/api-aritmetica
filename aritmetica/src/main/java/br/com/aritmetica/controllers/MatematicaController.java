package br.com.aritmetica.controllers;


import br.com.aritmetica.DTO.EntradaDTO;
import br.com.aritmetica.DTO.RespostaDTO;
import br.com.aritmetica.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO){
        regraEntrada(entradaDTO);
        RespostaDTO respostaDTO = matematicaService.soma(entradaDTO);
        return respostaDTO;
    }
    @PutMapping("/divisao")
    public RespostaDTO dividi(@RequestBody EntradaDTO entradaDTO){
        regraEntrada(entradaDTO);
        if(entradaDTO.getNumeros().indexOf(0) < entradaDTO.getNumeros().indexOf(1)){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, "Erro no calculo, entre primeiro com um numero maior, depois o menor");
        }
        RespostaDTO respostaDTO = matematicaService.dividi(entradaDTO);
        return respostaDTO;
    }
    @PutMapping("/multiplicacao")
    public RespostaDTO multiplica(@RequestBody EntradaDTO entradaDTO){
        regraEntrada(entradaDTO);
        RespostaDTO respostaDTO = matematicaService.multiplica(entradaDTO);
        return respostaDTO;
    }
    @PutMapping("/subtracao")
    public RespostaDTO subtrai(EntradaDTO entradaDTO){
        regraEntrada(entradaDTO);
        RespostaDTO respostaDTO = matematicaService.subtrai(entradaDTO);
        return respostaDTO;
    }

    public void regraEntrada(EntradaDTO entradaDTO){
        if(entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie dois numeros");
        }
        if(entradaDTO.getNumeros().indexOf(0) <= -1 && entradaDTO.getNumeros().indexOf(1) <= -1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie apenas números naturais");
        }

    }
}
